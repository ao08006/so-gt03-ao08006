.data

a: .long 0
b: .long 0
salida: .ascii "Resultado: %d\n"
tipo: .ascii "%d"
s1: .ascii "Ingrese el primer numero: \0"
s2: .ascii "Ingrese el segundo numero: \0"
.data
.global main
main:
	pushl $s1	#num1
	call printf
	addl $4, %esp
	pushl $a
	pushl $tipo
	call scanf
	addl $4, %esp
	
	pushl $s2
	call printf
	addl $4, %esp
	pushl $b
	pushl $tipo
	call scanf
	addl $8, %esp

	movl a, %eax	#suma
	addl b, %eax	#suma
	pushl %eax
	pushl $salida
	call printf
	addl $4, %esp
	movl $0, %ebx
	movl $1, %eax
	int $0x80
